/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.fileinputoutput;

import java.io.Serializable;
import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author 66955
 */
public class Computer implements Serializable{

    private int Hand; //hand : 0 , rock : 1 , paper : 2
    private int PlayerHand;//playerhand : 0 , rock : 1 , paper : 2
    private int Win, Lose, Draw,Status;

    public Computer() {

    }

    private int Choob() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int PawYingChoob(int PlayerHand) { //win :1 draw : 0 lose : -1
        this.PlayerHand = PlayerHand;
        this.Hand = Choob();
        if (this.PlayerHand == this.Hand) {
            Draw++;
            Status = 0;
            return 0;
        }
        if (this.PlayerHand == 0 && this.Hand == 1) {
            Win++;
             Status = 1;
            return 1;
        }
        if (this.PlayerHand == 1 && this.Hand == 2) {
            Win++;
             Status = 1;
            return 1;
        }
        if (this.PlayerHand == 2 && this.Hand == 0) {
            Win++;
             Status = 1;
            return 1;
        }
        Lose++;
         Status = -1;
        return -1;
        
    }

    public int getHand() {
        return Hand;
    }

    public int getPlayerHand() {
        return PlayerHand;
    }

    public int getWin() {
        return Win;
    }

    public int getLose() {
        return Lose;
    }

    public int getDraw() {
        return Draw;
    }

        public int getStatus() {
        return Status;
    }
}
